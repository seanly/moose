# Moose

Python API for users and devices registration management.

For usage as a consumer of APIs, check https://drone.beta.gouv.fr/developers/


## Development

* install `sqlite`
* create a virtualenv and install `requirements-dev.txt`
* set the `FLASK_APP` environment variable to `moose/__init__.py`
* run `flask initdb`
* run `flask run`

At this point, running `http :5000/device` with
[httpie](https://httpie.org/) should return a `403`.
