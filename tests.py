import json

import pytest
from flask import session, url_for
from flask.testing import FlaskClient

from moose.app import app as myapp
from moose.app import Device, Owner


class Client(FlaskClient):

    def open(self, *args, **kwargs):
        if 'content_type' not in kwargs:
            kwargs['content_type'] = 'application/json'
        if kwargs.get('content_type') == 'application/json':
            if 'data' in kwargs:
                kwargs['data'] = json.dumps(kwargs['data'])
        return super().open(*args, **kwargs)


myapp.test_client_class = Client


@pytest.fixture
def app():
    return myapp


@pytest.fixture
def owner(request_ctx):
    owner = Owner.create(franceconnect_id='1234', fullname='Testy Tester',
                         username='tester')
    request_ctx.g.user_details = {'sub': owner.id}
    yield owner
    owner.delete_instance()


@pytest.fixture
def owner2():
    owner2 = Owner.create(franceconnect_id='5678', fullname='Testy Two',
                          username='tester2')
    yield owner2
    owner2.delete_instance()


@pytest.fixture
def device(owner):

    def factory(**kwargs):
        data = {
            'name': 'test',
            'manufacturer': 'test',
            'model': 'test',
            'owner': owner.id,
            'has_camera': True,
            'weight': 800,
        }
        data.update(**kwargs)
        return Device.create(**data)
    return factory


def test_device_has_camera_should_default_to_false(owner):
    device = Device.create(name="test", manufacturer="test", model="test",
                           owner=owner, weight=800)
    assert device.has_camera is False


def test_get_user_should_return_a_user(owner, client):
    response = client.get(url_for('get_user', id=owner.id))
    assert response.json == owner.as_dict


def test_get_user_with_wrong_id_should_return_a_404(owner, client):
    response = client.get(url_for('get_user', id=888))
    assert response.status_code == 404
    assert response.json == {'description': 'Wrong user id'}


def test_get_device_should_return_a_device(device, client):
    drone = device()
    response = client.get(
        url_for('get_device', registration=drone.registration))
    assert response.json == drone.as_dict


def test_get_device_with_wrong_registration_should_return_a_404(owner, client):
    response = client.get(url_for('get_device', registration='wrong'))
    assert response.status_code == 404
    assert response.json == {'description': 'Wrong registration number'}


def test_head_device_should_return_a_device(device, client):
    drone = device()
    response = client.head(
        url_for('head_device', registration=drone.registration))
    assert response.status_code == 204


def test_head_device_with_wrong_registration_should_return_a_404(owner,
                                                                 client):
    response = client.head(url_for('head_device', registration='wrong'))
    assert response.status_code == 404


def test_register_post_without_data(client, owner):
    session['owner_id'] = owner.id
    session['oauth_token'] = '1234'
    response = client.post(url_for('post_device'))
    assert response.status_code == 400


def test_register_post_with_wrong_content_type(client, owner):
    response = client.post(url_for('post_device'), data={
        'name': 'foo',
        'owner': owner.id,
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
        'has_camera': 1
    }, content_type='text/html')
    assert response.status_code == 400
    assert response.json['description'] == 'No json body found'


def test_register_post_not_enough_data(client, owner):
    response = client.post(url_for('post_device'), data={'name': 'foo'})
    assert response.status_code == 400


def test_register_post_not_logged_in(client):
    response = client.post(url_for('post_device'), data={'name': 'foo'})
    assert response.status_code == 403


def test_register_post_with_data(client, owner):
    response = client.post(url_for('post_device'), data={
        'name': 'foo',
        'owner': owner.id,
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
        'has_camera': 1
    })
    assert response.status_code == 201
    assert response.json['name'] == 'foo'
    assert 'id' in response.json
    assert 'registration' in response.json


def test_register_post_with_data_and_registration(client, owner):
    response = client.post(url_for('post_device'), data={
        'name': 'foo',
        'owner': owner.id,
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
        'has_camera': 1,
        'registration': 'quux'
    })
    assert response.status_code == 400


def test_register_post_without_has_camera(client, owner):
    response = client.post(url_for('post_device'), data={
        'name': 'foo',
        'owner': owner.id,
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
        'has_camera': None,
    })
    assert response.status_code == 201
    assert response.json['has_camera'] is False


def test_register_cant_patch_on_point(client, owner):
    response = client.patch(url_for('post_device', id=123))
    assert response.status_code == 405


def test_register_patch_with_unknown_id_should_be_404(client, owner):
    response = client.patch(url_for('patch_device', id=123))
    assert response.status_code == 404
    assert response.json['description'] == 'Device with id 123 does not exist'


def test_patch_device(client, device, owner):
    drone = device(name='old_name')
    response = client.patch(url_for('patch_device', id=drone.id), data={
        'name': 'new_name'})
    assert response.status_code == 204
    assert Device.get(Device.id == drone.id).name == 'new_name'


def test_delete_device_with_unknown_id_should_be_404(client, device, owner):
    response = client.delete(url_for('delete_device', id=123))
    assert response.status_code == 404


def test_delete_device_without_being_logged_in_should_be_403(client, device):
    drone = device()
    response = client.delete(url_for('delete_device', id=drone.id))
    assert response.status_code == 403


def test_delete_device(client, device, owner):
    drone = device()
    response = client.delete(url_for('delete_device', id=drone.id))
    assert response.status_code == 204
    assert Device.select(Device.id == drone.id).count() == 0


def test_cannot_patch_registration(client, device, owner):
    drone = device()
    response = client.patch(url_for('patch_device', id=drone.id), data={
        'registration': 'xxxxx'})
    assert response.status_code == 400
    assert Device.get(Device.id == drone.id).registration == drone.registration


def test_mydevices(client, owner, owner2):
    Device.create(**{
        'name': 'foo1',
        'owner': owner.id,
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
        'has_camera': 1
    })
    Device.create(**{
        'name': 'foo2',
        'owner': owner2.id,
        'manufacturer': 'bar',
        'model': 'baz',
        'weight': '799',
        'has_camera': 1
    })
    response = client.get(url_for('get_device_list'))
    assert response.status_code == 200
    assert len(response.json['data']) == 1  # Only MY devices.
    assert response.json['data'][0]['name'] == 'foo1'
