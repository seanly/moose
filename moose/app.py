import json
import logging
import os
import uuid

import click
import peewee
from flask import Flask, Response
from flask_cors import CORS
from werkzeug.exceptions import HTTPException

get_env = os.environ.get

app = Flask(__name__)
app.config.update(
    SECRET_KEY=get_env('SECRET_KEY', 'sikr3t'),
)
CORS(app, supports_credentials=True)

logger = logging.getLogger('moose')
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
logger.addHandler(handler)

db = peewee.SqliteDatabase(get_env('DB_PATH', 'moose.db'))


class Owner(peewee.Model):
    franceconnect_id = peewee.CharField(unique=True)
    username = peewee.CharField(null=True)
    fullname = peewee.CharField()

    class Meta:
        database = db

    @property
    def as_dict(self):
        return {
            'id': self.id,
            'username': self.username,
            'fullname': self.fullname,
        }


class Device(peewee.Model):
    name = peewee.CharField(null=False)
    owner = peewee.ForeignKeyField(Owner, related_name='devices')
    manufacturer = peewee.CharField()
    model = peewee.CharField()
    weight = peewee.IntegerField()
    registration = peewee.CharField()
    has_camera = peewee.BooleanField(default=False)

    class Meta:
        database = db

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.registration:
            self.registration = uuid.uuid4().hex
        return super().save(*args, **kwargs)

    @property
    def as_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'owner': self.owner.id,
            'manufacturer': self.manufacturer,
            'model': self.model,
            'weight': self.weight,
            'registration': self.registration,
            'has_camera': self.has_camera,
        }


def abort(code, description):
    # Flask-like abort function, but with a json response.
    description = json.dumps({'description': description})
    response = Response(status=code, mimetype='application/json',
                        response=description)
    raise HTTPException(description=description, response=response)


@app.cli.command()
def initdb():
    """Initialize the database."""
    click.echo('Init the db')
    db.create_tables([Owner, Device], safe=True)
