import json
from http import HTTPStatus

import peewee
from coq.flask_coq import auth_required
from flask import jsonify, request

from .app import Device, abort, app, logger
from .auth import fc, get_current_user


@app.route('/device/<registration>', methods=['HEAD'])
def head_device(registration):
    exists = (Device.select().where(Device.registration == registration)
                    .exists())
    return '', exists and HTTPStatus.NO_CONTENT or HTTPStatus.NOT_FOUND


@app.route('/device/<registration>')
# @auth_required(fc)  # Deactivated for demo purpose, TODO: reactivate.
def get_device(registration):
    try:
        device = Device.get(Device.registration == registration)
    except Device.DoesNotExist:
        abort(404, description='Wrong registration number')
    return jsonify(device.as_dict)


@app.route('/device', methods=['POST'])
@auth_required(fc)
def post_device():
    try:
        data = dict(request.json)
    except TypeError:
        abort(400, description='No json body found')
    logger.debug('data %s', data)
    if not data:
        abort(400, description='Empty data')
    if 'registration' in data:
        abort(400, description='Registration should not be passed')

    data['owner'] = get_current_user()
    data['has_camera'] = bool(data.get('has_camera', False))
    try:
        device = Device.create(**data)
    except peewee.IntegrityError as e:
        abort(400, description='Missing data {}'.format(e))
    return json.dumps(device.as_dict), 201


@app.route('/device/<int:id>', methods=['PATCH'])
@auth_required(fc)
def patch_device(id):
    try:
        device = Device.get(Device.id == id,
                            Device.owner == get_current_user())
    except Device.DoesNotExist:
        abort(404, description='Device with id {} does not exist'.format(id))
    logger.debug('PATCH with %s for id %s', request.json, id)
    if not request.json:
        abort(400, description='Empty data')
    if 'registration' in request.json:
        abort(400, description='Registration should not be passed')

    for key, value in request.json.items():
        setattr(device, key, value)
    try:
        device.save()
    except peewee.IntegrityError as e:
        abort(400, description='Missing data {}'.format(e))
    return '', 204


@app.route('/device/<int:id>', methods=['DELETE'])
@auth_required(fc)
def delete_device(id):
    try:
        device = Device.get(Device.id == id,
                            Device.owner == get_current_user())
    except Device.DoesNotExist:
        abort(404, description='Device with id {} does not exist'.format(id))
    logger.debug('DELETE device %s', id)
    device.delete_instance()
    return '', 204


@app.route('/device')
@auth_required(fc)
def get_device_list():
    devices = Device.select().filter(Device.owner == get_current_user())
    return jsonify({'data': [device.as_dict for device in devices]})
