from moose.app import Device, Owner


def pytest_runtest_teardown():
    Device.delete().execute()
    Owner.delete().where(Owner.username == 'demo2').execute()
