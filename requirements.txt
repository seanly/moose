Flask==0.12.2
Flask-Cors==3.0.2
Flask-OAuthlib==0.9.3
peewee==2.10.1
-e git+https://framagit.org/drone/coq.git#egg=coq
